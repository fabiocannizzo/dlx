#include "dlx.h"
#include "adapter.h"

#include <iostream>
#include <algorithm>
#include <array>
#include <vector>

using namespace std;

/*
    Solving the simple binary cover problem.
    With the numbers {0, 1, 2} we need to fill a 2x1 grid so that
    each numbers appears only ones in the grid.

    There are 6 possible solutions:

    ---------    ---------     ---------    ---------     ---------    ---------
    | 0 | 1 |    | 1 | 0 |     | 0 | 2 |    | 2 | 0 |     | 1 | 2 |    | 2 | 1 |
    ---------    ---------     ---------    ---------     ---------    ---------

    The possibilities are:
      0: there is a 0 in cell 0
      1: there is a 0 in cell 1
      2: there is a 1 in cell 0
      3: there is a 1 in cell 1
      4: there is a 2 in cell 0
      5: there is a 2 in cell 1

    The constrainst are:
      required constraints:
        X0: cell 0 must contain either of {0,1,2}
        X1: cell 1 must contain either of {0,1,2}
      optional constraints:
        R0: row 0 can contain no more than one 0
        R1: row 0 can contain no more than one 1
        R2: row 0 can contain no more than one 2
 */

using dlx_t = dlx::DLX<dlx::CRS, false>;

void printSolution(const dlx_t* dlx)
{
    cout << "solution found\n";
    // show the indices of the chosen rows
    cout << "The chosen possibilities (i.e. rows) are:\n";
    vector<size_t> rowind;
    dlx->getSolutionRowIndices(rowind);
    for (auto i : rowind)
        cout << i << ", ";
    cout << "\n";

    // show the content of the chosen rows (only column indices of active elements are shown)
    cout << "which satisfy the constraints (i.e. the columns):\n";
    vector<vector<size_t>> sol;
    dlx->getSolution(sol);
    for (auto& r : sol) {
        for (auto& c : r)
            cout << c << ", ";
        cout << "\n";
    }
    cout << endl;
}

template <size_t nR, size_t nC>
struct Matrix
{
    array<array<bool, nC>, nR> m;
    array<bool,nC>& operator[](size_t r) { return m[r]; }
    bool operator()(size_t r, size_t c) const { return m[r][c]; }
    size_t nCols() const { return nC; }
    size_t nRows() const { return nR; }
};

int main()
{
    Matrix<6,5> m;
    //      X0 X1 R0 R1 R2
    m[0] = { 1, 0, 1, 0, 0 };  // 0 in cell 0 -> fills R0 X0
    m[1] = { 0, 1, 1, 0, 0 };  // 0 in cell 1 -> fills R0 X1
    m[2] = { 1, 0, 0, 1, 0 };  // 1 in cell 0 -> fills R1 X0
    m[3] = { 0, 1, 0, 1, 0 };  // 1 in cell 1 -> fills R1 X1
    m[4] = { 1, 0, 0, 0, 1 };  // 2 in cell 0 -> fills R2 X0
    m[5] = { 0, 1, 0, 0, 1 };  // 2 in cell 1 -> fills R2 X1

    // allocate sparse matrix
    dlx_t::crs_t crs{ m };

    // print the sparse input matrix
    for (size_t r = 0; r < m.nRows(); ++r) {
        for (size_t c = 0; c < m.nCols(); ++c)
            std::cout << m(r, c) << " ";
        std::cout << "\n";
    }
    std::cout << "\n";

    bool required[] = {true, true, false, false, false};

    // initialize the solver
    dlx_t s{ crs, required };

    // solve the problem
    auto n = s.solve(0, printSolution);

    cout << n << " solutions found\n";

    return 0;
}
