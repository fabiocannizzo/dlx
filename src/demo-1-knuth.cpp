#include "dlx.h"
#include "adapter.h"

#include <iostream>

/*
    Solving the simple binary cover problem illustrated in Donald Knuth's paper.
    We use as input a matrix of bool values defined as vector<vector<bool>>.
    The possibilities are in rows, the constraits are columns.
    We use an dapter to convert the matrix to the format required by dlx.
*/


int main()
{
    // example taken from Knuth paper
    std::vector<std::vector<bool>> m(6);
    m[0] = { 0, 0, 1, 0, 1, 1, 0 };
    m[1] = { 1, 0, 0, 1, 0, 0, 1 };
    m[2] = { 0, 1, 1, 0, 0, 1, 0 };
    m[3] = { 1, 0, 0, 1, 0, 0, 0 };
    m[4] = { 0, 1, 0, 0, 0, 0, 1 };
    m[5] = { 0, 0, 0, 1, 1, 0, 1 };

    // print the input matrix
    for (auto& r : m) {
        for (auto v : r)
            std::cout << v << " ";
        std::cout << "\n";
    }
    std::cout << "\n";

    // initialize the solver
    dlx::DLX<dlx::CRS,false> s{ dlx::CRS{dlx::adapter::VecVec<bool>{m}} };

    // solve the problem
    s.solve(1);

    // show the indices of the chosen rows
    std::cout << "The chosen possibilities (i.e. rows) are:\n";
    std::vector<size_t> rowind;
    s.getSolutionRowIndices(rowind);
    for (auto i : rowind)
        std::cout << i << ", ";
    std::cout << "\n";

    // show the content of the chosen rows (only column indices of active elements are shown)
    std::cout << "which satisfy the constraints (i.e. the columns):\n";
    std::vector<std::vector<size_t>> sol;
    s.getSolution(sol);
    for (auto& r : sol) {
        for (auto& c : r)
            std::cout << c << ", ";
        std::cout << "\n";
    }

    return 0;
}
