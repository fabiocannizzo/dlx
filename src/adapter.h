#pragma once

#include <vector>

namespace dlx::adapter
{
    template < typename T
             , typename AT = typename std::allocator<T>
             , typename AV = typename std::allocator<std::vector<T, AT>>
             >
    class VecVec
    {
        typedef std::vector<std::vector<T, AT>, AV> type;
        const type& ref;
    public:
        VecVec(const type& v) : ref(v) {}
        size_t nRows() const { return ref.size(); }
        size_t nCols() const { return ref[0].size(); }
        bool operator()(size_t r, size_t c) const { return ref[r][c]; }
    };

} // namespace dlx::adapter


