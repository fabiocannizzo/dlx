#pragma once

#include "crs.h"

#include <vector>
#include <limits>
#include <algorithm>
#include <cassert>
#include <random>
#include <type_traits>
#include <functional>

namespace dlx
{

/*
    CRS must implement:
        size_t nnz() const
        size_t nRows() const
        size_t nCols() const
        size_t rowlen(size_t r)
        size_t rowpos(size_t r)
        const size_t* rowptr(size_t r) const
*/
template <typename CRS, bool Randomize = false> // randomize order of rows per column
class alignas(128) DLX
{
public:
    typedef CRS crs_t;
    typedef std::function<void(const DLX *)> callback_t;

private:
    struct ColHeader;

    struct Node
    {
    private:
        template <Node * Node:: * prev, Node * Node:: * next>
        void remove()
        {
            auto pr = this->*prev;
            auto nx = this->*next;
            pr->*next = nx;
            nx->*prev = pr;
            if constexpr (prev == &Node::up)
                head->m_n--;
        }

        template <Node * Node:: * prev, Node * Node:: * next>
        void reinsert()
        {
            this->*prev->*next = this->*next->*prev = this;
            if constexpr (prev == &Node::up)
                head->m_n++;
        }

    public:
        Node* left, *right, *up, *down;
        ColHeader* head;

        // remove this horizontally
        void hRemove() { remove<&Node::left, &Node::right>(); }
        // reinsert this horizontally
        void hReinsert() { reinsert<&Node::left, &Node::right>(); }

        bool vNotCovered() const { return up->down == this; }
        bool hNotCovered() const { return right->left == this; }

        // remove this vertically
        void vRemove() { remove<&Node::up, &Node::down>(); }
        // reinsert this vertically
        void vReinsert() { reinsert<&Node::up, &Node::down>(); }

        // insert this horizontally before node
        void vInsBefore(Node *node)
        {
            this->up = node->up;
            this->down = node;
            vReinsert();
        }
    };

    struct ColHeader : Node
    {
        size_t m_n;
        bool m_required;

        bool notCovered() const { return Node::hNotCovered(); }

        void cover()
        {
            Node::hRemove();
            for (Node* i = this->down; i != this; i = i->down)
                for (Node* j = i->right; j != i; j = j->right)
                    j->vRemove();
        }

        void uncover()
        {
            for (Node* i = this->up; i != this; i = i->up)
                for (Node* j = i->left; j != i; j = j->left)
                    j->vReinsert();
            Node::hReinsert();
        }
    };

    struct NodesOrderWithinEachColumn
    {
        bool initialized() const {
            return !m_colIndex.empty();
        }
        void init(std::vector<ColHeader>& colheads, std::vector<Node>& nodes)
        {
            std::random_device rnd;
            std::mt19937 urng(rnd());

            // initialize data structure and randomize order
            const size_t nCols = colheads.size() - 1;  // remember that colheads has size nCols + 1
            m_colIndex.resize(nCols + 1);
            m_colNodes.resize(nodes.size());
            m_colIndex[0] = 0;
            for (size_t c = 0; c < nCols; c++) {
                const ColHeader *head = &colheads[c];
                size_t colIndex = m_colIndex[c];
                m_colIndex[c + 1] = colIndex + head->n;
                auto colNodeIter = m_colNodes.begin() + colIndex;
                for (Node *pn = head->down; pn != head; pn = pn->down)
                    *colNodeIter++ = pn;

                // randomize
                shuffle(m_colNodes.begin() + m_colIndex[c], m_colNodes.begin() + m_colIndex[c + 1], urng);
            }
        }

        std::vector<size_t> m_colIndex; // colIndex[i] is the index of vector colNodes where the nodes of the i-th column start
        std::vector<Node *> m_colNodes; // pointers to nodes belonging to the i-th column in the desired order
    };
    class Dummy {};

    // link horizontally left node with right node
    static void hLink(Node *left, Node * right)
    {
        left->right = right;
        right->left = left;
    }
    static void hLink(Node* left, Node *mid, Node* right)
    {
        hLink(left, mid);
        hLink(mid, right);
    }

    CRS m_crs;
    std::vector<ColHeader> m_cols; // layout {head0, head1, ..., root}
    std::vector<Node> m_nodes;
    std::vector<Node*> m_solution; // pointers to nodes selected as part of the solution
    typename std::conditional<Randomize, NodesOrderWithinEachColumn, Dummy>::type m_nodesOrder; // order of nodes within each column (only used if Randomize)
    size_t m_nFound, m_nSought;
    callback_t m_callback;
    bool m_tableauDirty;
    bool m_done;
    bool m_allRequired;

    // heuristic which picks the column with the smallest number of rows
    template <bool AllRequired>
    ColHeader* _chooseColumn()
    {
        ColHeader* f = nullptr;
        size_t mi = std::numeric_limits<size_t>::max();
        auto *r = root();
        for (auto p = r->right; p != r; p = p->right) {
            ColHeader* h = static_cast<ColHeader*>(p);
            if (AllRequired || h->m_required) {
                const size_t n = h->m_n;
                if (n < mi) {
                    mi = n;
                    f = h;
                }
            }
            else  // not required constraints are at the end, if we found one we can break
                break;
        }
        return f;
    }

    ColHeader* chooseColumn()
    {
        if (m_allRequired)
            return _chooseColumn<true>();
        return _chooseColumn<false>();
    }

    Node *root() { return m_cols.data() + nCols(); }

public:

    size_t nCols() const { return m_crs.nCols(); }
    size_t nRows() const { return m_crs.nRows(); }

    DLX() {}

    explicit DLX(const CRS& matrix, const bool* required = nullptr)
        : m_crs(matrix)
    {
        reinit(required);
    }

    // this breaks encapsulation completely! After a change to the matrix a reinit is needed.
    CRS& crs() { return m_crs; }

    // to be called if the crs matrix has been modified
    void reinit(const bool* required = nullptr)
    {
        m_cols.resize(1 + nCols()); // +1 for the root (root is the last)
        m_allRequired = true;
        for (size_t i = 0; i < nCols(); ++i)
            m_allRequired &= (m_cols[i].m_required = !required || required[i]);
        m_nodes.resize(m_crs.nnz());
        m_tableauDirty = true;
        m_solution.reserve(nRows());
        reset();
    }

    void reset()
    {
        // is dirty?
        if (!m_tableauDirty)
            return;

        const auto nr = nRows();
        const auto nc = nCols();

        // init root and all column heads
        hLink(root(), root());
        for (ColHeader *pn = m_cols.data(), *pe = pn + nc; pn != pe; pn++) {
            pn->head = pn;
            pn->down = pn->up = pn;
            pn->m_n = 0;
            if (pn->m_required)
                hLink(root(), pn, root()->right);  // insert in front
            else
                hLink(root()->left, pn, root());   // append at the end
        }

        // init all values
        for (size_t r = 0, k = 0; r < nr; ++r) {
            size_t rowlen = m_crs.rowlen(r);
            assert(rowlen);
            const size_t *rowptr = m_crs.rowptr(r);
            for (const size_t *pc = rowptr, *pe = pc + rowlen; pc != pe; pc++) {
                Node *cur = &m_nodes[k++];
                size_t col = *pc;
                ColHeader *headptr = m_cols.data() + col;
                cur->head = headptr;
                cur->vInsBefore(headptr);
                if (pc != rowptr) // not the first
                    hLink(cur - 1, cur);
                else
                    hLink(cur + (rowlen - 1), cur);
            }
        }

        if constexpr (Randomize) {

            if (!m_nodesOrder.initialized())
                m_nodesOrder.init(m_cols, m_nodes);

            // convenience variables
            const auto& colIndex = m_nodesOrder.m_colIndex;
            const auto& colNodes = m_nodesOrder.m_colNodes;

            // now randomize in the tableau according with m_nodesOrder
            for (size_t c = 0; c < nc; c++) {
                auto *phead = &m_cols[c];
                for (auto ppn = colNodes.begin() + colIndex[c], ppend = ppn + phead->n; ppn != ppend; ppn++) {
                    Node *pn = *ppn;
                    pn->vRemove();
                    pn->vInsBefore(phead);
                }
            }
        }

        // reset solution
        m_solution.clear();

        m_tableauDirty = false;
    }

    // note this operation has some cost, you may want to cache the result
    void getSolutionRowIndices(std::vector<size_t>& solRowIndex) const
    {
        const size_t n = m_solution.size();
        solRowIndex.resize(n);

        auto *data = m_nodes.data();
        if constexpr (std::is_same<CRS, dlx::CRS>::value) {
            auto* rbegin = m_crs.vec_index().data();
            auto* rend = rbegin + m_crs.vec_index().size();
            for (size_t i = 0; i < n; i++) {
                size_t pos = std::distance<const Node*>(data, m_solution[i]);
                auto r = std::distance(rbegin, std::upper_bound(rbegin, rend, pos)) - 1;
                solRowIndex[i] = r;
            }
        }
        else { // the matrix has a constant number of non zero per row, and we can exploit of that to determine the row
            for (size_t i = 0; i < n; i++) {
                size_t pos = std::distance<const Node*>(data, m_solution[i]);
                auto r = pos / CRS::rowlen(0);
                solRowIndex[i] = r;
            }
        }
    }

    void getSolution(std::vector<std::vector<size_t>>& colIndices) const
    {
        colIndices.resize(m_solution.size());
        for (size_t r = 0; r < m_solution.size(); r++) {
            auto i = m_solution[r];
            auto& v = colIndices[r];
            auto p = i;
            do {
                v.push_back(std::distance<const ColHeader *>(&m_cols.front(), i->head));
                i = i->right;
            } while (i != p);
        }
    }

    void addToSolution(Node *r)
    {
        m_solution.push_back(r);
        for (Node* j = r->right; j != r; j = j->right)
            j->head->cover();
    }

    void removeFromSolution(Node *r)
    {
#if _DEBUG
        assert(r == m_solution.back());
#endif
        m_solution.pop_back();
        for (Node* j = r->left; j != r; j = j->left)
            j->head->uncover();
    }

    bool addRequirement(size_t row)
    {
        auto pos = m_crs.rowpos(row);
        auto *node0 = &m_nodes[pos];
        auto *node = node0;
        do {
            if (node->head->notCovered() && node->vNotCovered()) {
                m_tableauDirty = true;
                node->head->cover();
                addToSolution(node);
                return true;
            }
            node = node->right;
        } while (node != node0);
        return false;
    }

    void search(size_t k)
    {
        auto c = chooseColumn();
        if (c != nullptr) {
            if (c->m_n > 0) {
                c->cover();
                for (Node* r = c->down; r != c; r = r->down) {
                    addToSolution(r);
                    search(k + 1);
                    if (m_done)
                        return;
                    removeFromSolution(r);
                }
                c->uncover();
            }
            else {
                // unfeasible
                return;
            }
        }
        else {
            // solution found
            m_done = ++m_nFound == m_nSought;
            if (m_callback)
                m_callback(this);
        }
    }

    size_t solve(size_t n /* use zero if you want all solutions */, const callback_t& f = callback_t{})
    {
        m_tableauDirty = true;
        m_callback = f;
        m_nSought = n;
        m_nFound = 0;
        m_done = false;
        search(0);
        return m_nFound;
    }
};

} // namespace dlx
