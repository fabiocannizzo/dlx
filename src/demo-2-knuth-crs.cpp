#include "dlx.h"
#include "adapter.h"

#include <iostream>
#include <algorithm>

using namespace std;

/*
    Solving the simple binary cover problem illustrated in Donald Knuth's paper.
    This is similar to demo-1-kunth, except we construct the CRS matrix manually.
*/

struct RowInfo {
    template <size_t N>
    RowInfo(const size_t(&row)[N]) : rowptr(row), rowsize(N) {}

    const size_t* rowptr;
    size_t rowsize;
};

int main()
{
    // example taken from Knuth paper
    // { 0, 0, 1, 0, 1, 1, 0 };
    // { 1, 0, 0, 1, 0, 0, 1 };
    // { 0, 1, 1, 0, 0, 1, 0 };
    // { 1, 0, 0, 1, 0, 0, 0 };
    // { 0, 1, 0, 0, 0, 0, 1 };
    // { 0, 0, 0, 1, 1, 0, 1 };

    const size_t row0[] = {2, 4, 5};  // indices of columns with non zeros in row 0
    const size_t row1[] = {0, 3, 6};  // indices of columns with non zeros in row 1
    const size_t row2[] = {1, 2, 5};  // indices of columns with non zeros in row 2
    const size_t row3[] = {0, 3};     // indices of columns with non zeros in row 3
    const size_t row4[] = {1, 6};     // indices of columns with non zeros in row 4
    const size_t row5[] = {3, 4, 6};  // indices of columns with non zeros in row 5

    // for convenience create an array of pairs, containing the pointers to each row amd their size
    const RowInfo rows[] = { row0, row1, row2, row3, row4, row5 };

    // sparse matrix geometry
    const size_t ncols = 7;  // number of constraints
    const size_t nrows = 6;  // number of possibilities: 6
    const size_t nnz = 16;   // number of non-zero elements

    // allocate sparse matrix
    dlx::CRS m{ nrows, ncols, nnz };

    // fill the sparse matrix
    auto& row_index = m.vec_index();
    row_index[0] = 0;
    for (size_t r = 0; r < nrows; ++r) {
        size_t nc = rows[r].rowsize;
        row_index[r+1] = row_index[r] + nc;
        const size_t *cols = rows[r].rowptr;
        copy(cols, cols + nc, m.rowptr(r));
    }

    // print the sparse input matrix
    for (size_t r = 0; r < nrows; ++r) {
        for (size_t c = 0; c < ncols; ++c)
            std::cout << m(r,c) << " ";
        std::cout << "\n";
    }
    std::cout << "\n";

    // initialize the solver
    dlx::DLX<dlx::CRS,false> s{ m };

    // solve the problem
    s.solve(1);

    // show the indices of the chosen rows
    cout << "The chosen possibilities (i.e. rows) are:\n";
    vector<size_t> rowind;
    s.getSolutionRowIndices(rowind);
    for (auto i : rowind)
        cout << i << ", ";
    cout << "\n";

    // show the content of the chosen rows (only column indices of active elements are shown)
    cout << "which satisfy the constraints (i.e. the columns):\n";
    vector<vector<size_t>> sol;
    s.getSolution(sol);
    for (auto& r : sol) {
        for (auto& c : r)
            cout << c << ", ";
        cout << "\n";
    }

    return 0;
}
