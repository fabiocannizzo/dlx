#include "dlx.h"
#include "adapter.h"

#include <iostream>
#include <algorithm>
#include <array>
#include <vector>

using namespace std;

/*
    Solving the simple binary cover problem.
    With the numbers 0 and 1 we need to fill a 2x2 grid so that
    each numbers appears only ones in each row and in each column.

    There are two possible solutions:

    ---------    ---------
    | 0 | 1 |    | 1 | 0 |
    ---------    ---------
    | 1 | 0 |    | 0 | 1 |
    ---------    ---------

    The possibilities are:
      0: there is a 0 in (0,0)
      1: there is a 0 in (0,1)
      2: there is a 0 in (1,0)
      3: there is a 0 in (1,1)
      4: there is a 1 in (0,0)
      5: there is a 1 in (0,1)
      6: there is a 1 in (1,0)
      7: there is a 1 in (1,1)

    The constrainst are:
      C00: col 0 must contain 0
      C01: col 0 must contain 1
      C10: col 1 must contain 0
      C11: col 1 must contain 1
      R00: row 0 must contain 0
      R01: row 0 must contain 1
      R10: row 1 must contain 0
      R11: row 1 must contain 1
      X00: (0,0) must contain either 0 or 1
      X01: (0,1) must contain either 0 or 1
      X10: (1,0) must contain either 0 or 1
      X11: (1,1) must contain either 0 or 1
 */

using dlx_t = dlx::DLX<dlx::CRS, false>;

void printSolution(const dlx_t* dlx)
{
    cout << "solution found\n";
    // show the indices of the chosen rows
    cout << "The chosen possibilities (i.e. rows) are:\n";
    vector<size_t> rowind;
    dlx->getSolutionRowIndices(rowind);
    for (auto i : rowind)
        cout << i << ", ";
    cout << "\n";

    // show the content of the chosen rows (only column indices of active elements are shown)
    cout << "which satisfy the constraints (i.e. the columns):\n";
    vector<vector<size_t>> sol;
    dlx->getSolution(sol);
    for (auto& r : sol) {
        for (auto& c : r)
            cout << c << ", ";
        cout << "\n";
    }
    cout << endl;
}

template <size_t nR, size_t nC>
struct Matrix
{
    array<array<bool, nC>, nR> m;
    array<bool,nC>& operator[](size_t r) { return m[r]; }
    bool operator()(size_t r, size_t c) const { return m[r][c]; }
    size_t nCols() const { return nC; }
    size_t nRows() const { return nR; }
};

int main()
{
    Matrix<8,12> m;
    //      C00 C01 C10 C11 R00 R01 R10 R11 X00 X01 X10 X11
    m[0] = {  1,  0,  0,  0,  1,  0,  0,  0,  1,  0,  0,  0 };  // 0 in (0,0) -> fills R00 C00 X00
    m[1] = {  0,  0,  1,  0,  1,  0,  0,  0,  0,  1,  0,  0 };  // 0 in (0,1) -> fills R00 C10 X01
    m[2] = {  1,  0,  0,  0,  0,  0,  1,  0,  0,  0,  1,  0 };  // 0 in (1,0) -> fills R10 C00 X10
    m[3] = {  0,  0,  1,  0,  0,  0,  1,  0,  0,  0,  0,  1 };  // 0 in (1,1) -> fills R10 C10 X11
    m[4] = {  0,  1,  0,  0,  0,  1,  0,  0,  1,  0,  0,  0 };  // 1 in (0,0) -> fills R01 C01 X00
    m[5] = {  0,  0,  0,  1,  0,  1,  0,  0,  0,  1,  0,  0 };  // 1 in (0,1) -> fills R01 C11 X01
    m[6] = {  0,  1,  0,  0,  0,  0,  0,  1,  0,  0,  1,  0 };  // 1 in (1,0) -> fills R11 C01 X10
    m[7] = {  0,  0,  0,  1,  0,  0,  0,  1,  0,  0,  0,  1 };  // 1 in (1,1) -> fills R11 C11 X11

    // allocate sparse matrix
    dlx_t::crs_t crs{ m };

    // print the sparse input matrix
    for (size_t r = 0; r < m.nRows(); ++r) {
        for (size_t c = 0; c < m.nCols(); ++c)
            std::cout << m(r, c) << " ";
        std::cout << "\n";
    }
    std::cout << "\n";

    // initialize the solver
    dlx_t s{ crs };

    // solve the problem
    auto n = s.solve(0, printSolution);

    cout << n << " solutions found\n";

    return 0;
}
