#pragma once

#include <cstdint>
#include <vector>
#include <algorithm>

namespace dlx
{

// compressed row storage matrix of booleans
class CRS
{
    size_t m_nCols;
    std::vector<size_t> m_index;  // inidices of rows in m_nnz
    std::vector<size_t> m_nnz;    // indices of non zero elements

public:
    size_t nnz() const { return static_cast<size_t>(m_nnz.size()); }
    size_t nRows() const { return static_cast<size_t>(m_index.size() - 1); }
    size_t nCols() const { return m_nCols; }

    size_t rowlen(size_t r) const { return m_index[r + 1] - m_index[r]; }
    const size_t* rowptr(size_t r) const { return m_nnz.data() + m_index[r]; }
    size_t* rowptr(size_t r) { return m_nnz.data() + m_index[r]; }
    size_t rowpos(size_t r) const { return m_index[r]; }

    std::vector<size_t>& vec_index() { return m_index; }
    const std::vector<size_t>& vec_index() const { return m_index; }
    std::vector<size_t>& vec_nnz() { return m_nnz; }

    bool operator()(size_t r, size_t c) const
    {
        auto p = rowptr(r);
        auto s = rowlen(r);
        return std::binary_search(p, p + s, c);
    }

    CRS() : m_nCols(0) {}

    // FIXME: use concepts
    // type M must implement the functions:
    //    bool operator()(size_t, size_t);  // or any other return type convertible to bool
    //    size_t nRows();
    //    size_t nCols();
    template <typename M>
    explicit CRS(const M& matrix)
        : m_nCols(static_cast<size_t>(matrix.nCols()))
        , m_index(static_cast<size_t>(matrix.nRows() + 1))
    {
        // count non zero elements and initialize row indices
        m_index[0] = 0; // nodes commence after column heads and root
        for (size_t r = 0, re = nRows(); r < re; ++r) {
            for (size_t c = 0, ce = nCols(); c < ce; ++c) {
                if(matrix(r, c))
                    m_nnz.push_back(c);
            }
            m_index[r + 1] = static_cast<size_t>(m_nnz.size());
        }
    }

    explicit CRS(size_t nRows, size_t nCols, size_t nnz)
        : m_nCols(nCols)
        , m_index(nRows + 1)
        , m_nnz(nnz)
    {
    }

    void reinit(size_t nRows, size_t nCols, size_t nnz)
    {
        m_nCols = nCols;
        m_index.resize(nRows + 1);
        m_nnz.resize(nnz);
    }
};

// compressed row storage matrix of booleans
template <size_t NumColsPerRow>
class CRSRegular // has constant number of rows per columns
{
    size_t m_nCols;
    size_t m_nRows;
    std::vector<size_t> m_nnz;    // indices of non zero elements

public:
    size_t nnz() const { return static_cast<size_t>(m_nnz.size()); }
    size_t nRows() const { return m_nRows; }
    size_t nCols() const { return m_nCols; }

    static constexpr size_t rowlen(size_t r) { return NumColsPerRow; }
    static constexpr size_t rowpos(size_t r) { return r * NumColsPerRow; }
    const size_t* rowptr(size_t r) const { return m_nnz.data() + rowpos(r); }
    size_t* rowptr(size_t r) { return m_nnz.data() + rowpos(r); }

    std::vector<size_t>& vec_nnz() { return m_nnz; }

    CRSRegular() : m_nCols(0), m_nRows(0) {}

    explicit CRSRegular(size_t nRows, size_t nCols, size_t nnz)
        : m_nCols(nCols)
        , m_nRows(nRows)
        , m_nnz(nnz)
    {
    }

    void reinit(size_t nRows, size_t nCols, size_t nnz)
    {
        m_nCols = nCols;
        m_nRows = nRows;
        m_nnz.resize(nnz);
    }
};

} // namespace dlx