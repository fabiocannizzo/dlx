.SUFFIXES:

CC=g++

BINDIR=bin-$(CC)

CFLAGS += -c -O3 -std=c++17 -msse4 -DNDEBUG -I./
LFLAGS +=

# platform
PLATFORM := $(shell uname -s)

$(info platform=$(PLATFORM))
$(info compiler=$(CC))


# file extensions
ifeq ($(PLATFORM), Linux)
  OBJ=.o
  EXE=
  DLL=.so
  LIB=.a
else
# cygwin or mingw
  OBJ=.obj
  EXE=.exe
  DLL=.dll
  LIB=.lib
endif

ifeq ($(PLATFORM), Linux)
   LFLAGS += -pthread
endif
SRC=$(wildcard src/*.cpp)
PROG=$(wildcard src/demo-*.cpp)
TEST=$(wildcard src/test-*.cpp)
HEADERS=$(wildcard src/*.h)
DEPS=$(HEADERS) Makefile

TARGET_PROG=$(patsubst src/%.cpp, $(BINDIR)/%$(EXE), $(PROG))
TARGET_TEST=$(patsubst src/%.cpp, $(BINDIR)/%$(EXE), $(TEST))
#OBJ=$(patsubst src/%.cpp, $(BINDIR)/%.o, $(SRC))


# compile, link and encode
all: $(TARGET_PROG) $(TARGET_TEST)

$(BINDIR)/%.i : src/%.cpp $(DEPS) | $(BINDIR)
	$(CC) -E $(CFLAGS) $< -o $@

$(BINDIR)/%.asm : src/%.cpp $(DEPS) | $(BINDIR)
	$(CC) -g -Wa,-adhln -masm=intel $(CFLAGS) $< > $@

$(BINDIR)/%$(OBJ): src/%.cpp $(DEPS) | $(BINDIR)
	$(CC) $(CFLAGS) -o $@ $<

$(BINDIR)/%$(EXE): $(BINDIR)/%$(OBJ) | $(BINDIR)
	$(CC) $(LFLAGS) $< -o $@


# clean
.PHONY: clean
clean:
	rm -rf bin-*


# use -p for multithreading
.PHONY: $(BINDIR)
$(BINDIR):
	mkdir -p $(BINDIR)
